import streamlit as st

import pandas as pd
import itkdb
import itkdb.exceptions as itkX

import pandas as pd
import altair as alt
import json


import sys
import os

# caution: path[0] is reserved for script path (or '' in REPL)
sys.path.insert(1, os.getcwd()+'/pdbApp/scripts')
from commonCode import GetPDBAccess, SelectComponentType, SelectTestTypeOEC, EditSchema, SelectCheck




### introduction
st.title(':clock230: Half-Ring Stage Status')
st.write('### Check tests performed and tests remaining')
st.write("---")

### input user info.
st.write("## Get PDB Token")
st.write("Input user credentials to get token")

if "client" in st.session_state.keys():
    st.write("__token already found__")
else:
    GetPDBAccess(st.session_state)

if "client" not in st.session_state.keys():
    st.write("Please register credentials")
    st.stop()

st.write("---")



st.write('## Check test status')

st.write("## Available half-rings")

# get all componentTypes then filter
# if "compTypeList" not in st.session_state.keys():
#     st.session_state['compTypeList']=st.session_state['client'].get('listComponentTypes', json={'project':st.session_state['project'][0], 'componentType':"MODULES" }).data
client = st.session_state['client']
user = client.get("getUser", json={"userIdentity": client.user.identity})
user_location = user["institutions"][0]["code"]

data = {"project": "P", "subproject":"PE"}  
components_pixels = client.get("listComponents", json=data)  

relevantSN = [] #store half-ring objects
for component in components_pixels:
    
    if type is None: #make sure to parse out NoneType objects
        pass

    else:
        sn = str(component["serialNumber"])
        if "20UPELS" in sn:
            if component["state"] == "ready": #the other opetion is request to delete 
                #st.write(component["type"]["code"])
                location = component['institution']['code']
                if location == user_location: #collect at your institute
                    relevantSN.append(sn)

option = st.selectbox('Please select half-ring SN', (relevantSN))
selComponent = client.get("getComponent", json={"component":option})
st.write(f'Your component is in stage {selComponent["currentStage"]["name"]}')

test_types = client.get("listTestTypes", data={"project":"P", "componentType":"OEC_LLS"})

########### UNDER CONSTRUCTION #################

compTypeInfo=client.get('getComponentTypeByCode', json={'code':'OEC_LLS','project':'P'})
compInfo = client.get('getComponent', json={'component':option})

df_stageList=pd.DataFrame(compTypeInfo['stages'])
df_stageList=df_stageList.rename(columns={k:'stage_'+k for k in ['code','name','order','alternative','initial','final']})

stageList= df_stageList['stage_code'].tolist()
stageTests = df_stageList['testTypes']

currentStage = selComponent['currentStage']['code']
stageTests_codes = [] #keep codes of tests corresponding to current stage
stageTests_names = [] #keep stage of tests corresponding to current stage

for stage in range(len(stageList)):

    if currentStage == stageList[stage]:


        assTest = stageTests[stage]

        if len(assTest) > 0:

            for tt in range(len(assTest)):

                stageTests_codes.append(assTest[tt]['testType']['code'])
                stageTests_names.append(assTest[tt]['testType']['name'])

        else:
            st.write('There are no test types associated with this stage')

st.write('The tests associated with this stage are:')

for name in range(len(stageTests_names)):

    st.write(f'- ', stageTests_names[name])

st.write('## Tests performed info')
tests_done_id = [] #collect IDs of collected tests

for test in selComponent['tests']:

    name = test["code"]
    for code in range(len(stageTests_codes)):

        corres_name = stageTests_names[code]
        if stageTests_codes[code] == name:  #have you done this test before?


            for run in test["testRuns"]:
                 if run["state"] == "ready": #i.e. not requestToDelete
                     if run["passed"] == True:
                         st.write(f":white_check_mark: Component passed {corres_name} on {run['stateTs'][:10]}")

                     elif run["passed"] == False:
                         
                         st.write(f":no_entry_sign: Component failed {corres_name} on {run['stateTs'][:10]}")




