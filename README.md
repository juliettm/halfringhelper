# PDB webApp example

Example of useful webApp functionality

Contents:

 * [Running example code](#running-example-code)
 * [Generating schemas for data upload](#generating-schemas-for-data-upload)
 * [Basic reporting](#basic-reporting)
 * [Population Definition](#population-definition)
 * [Parameter Extraction](#parameter-extraction)
 * [Visualisation](#visualisation)
 * [Distribution](#distribution)

<small><i><a href='http://ecotrust-canada.github.io/markdown-toc/'>Table of contents generated with markdown-toc</a></i></small>

## Running example code

From repository top directory:

 - __local running__
 > streamlit run pdbApp/pdbApp.py --server.port=8501

 - __docker running__

    - build image
    > docker build -f dockerFiles/Dockerfile_pdbApp -t pdb-app .

    - run with local directories mounted
    > docker run -p 8501:8501 -v $(pwd)/pdbApp/:/code/pdbApp/ pdb-app

Open app in browser with localhost:8501

## Generating schemas for data upload

To keep database consistent the API will only accept uploads which follow a strict format. Uploaded data must conform to a strict _json_ schema with expected key-value format.

The expected _json_ schemas for data uploads to ITk PDB can be queried:

 - componentTypes: generateComponentTypeDtoSample with 
 > json={'project':"PROJECT_CODE", 'code':"COMPONENTTYPE_CODE"}
 - testTypes: generateTestTypeDtoSample with 
 > json={'project':"PROJECT_CODE", 'componentType':"COMPONENTTYPE_CODE", 'code':"TESTTYPE_CODE"}

__NB__ optional parameters maybe excluded/included using the _requiredOnly_ flag

For example (strips module metrology):

``` python
test_obj = client.get('generateTestTypeDtoSample', json={'project':"S", 'componentType':"MODULE", 'code':"MODULE_METROLOGY"} 'requiredOnly':True} )
```

The returned json object can then be populated and uploaded to the ITk PDB.


## Basic reporting

<!-- Reporting documentation exists elsewhere: [itk-reports]() -->

Database reporting gathers information from the database on a population of objects.

The basic components:

 - define population, e.g. pixels sensors
 - define parameter(s) to extract information, e.g. depletion voltage
 - define visualisation, e.g. table of averages per type, histogram
 - (distribution, i.e. how to share the information)


## Population Definition

Lists of objects can be returned using _listComponents_, _getComponentBulk_ and _getTestRunBulk_ commands.

Simple example (<1000 components in PDB):

``` python
compList=client.get('listComponents', json={'project':"PROJECT_CODE",'componentType':"COMPONENTTYPE_CODE"})
```
__NB__ By default the returned data is paginated. This avoid long periods of PDB interact which can result in timeout errors. This simple command will return up to 1000 components. 
To get all components the requests:

 1. Check component count:
 > total=client.get('getComponentCount', json={'project':"PROJECT_CODE",'componentType':"COMPONENTTYPE_CODE"}})['count']
 2. Loop over pages:
    ``` python
    pageSize=100
    count= int(total/pageSize)
    ### catch the remainder
    if total%pageSize>0:
        count=count+1
    ### retrieving components..."
    popList=[]
    for pi in range(0,count,1):
        ### get list from page pi
        compList=myClient.get('listComponents', json={'project':"PROJECT_CODE",'componentType':"COMPONENTTYPE_CODE",'pageInfo':{ 'pageIndex': pi, 'pageSize': pageSize }})
        ### add to list
        popList.extend( compList.data )
    ```


## Parameter Extraction

Check available data for database objects using _getComponentTypeByCode_ and _getTestTypeByCode_ commands.

This will return the generic information of the object.

Generically:

- componentType
``` python
compList=client.get('getComponentTypeByCode', json={'project':"PROJECT_CODE",'code':"COMPONENTTYPE_CODE"})
```
    - returning: stages, types, parents, children, properties

- testType
``` python
compList=client.get('getTestTypeByCode', json={'project':"PROJECT_CODE",'componentType':"COMPONENTTYPE_CODE", 'code':"TESTTYPE_CODE" })
```
    - returning: properties, results (_aka_ parameters)

In both cases properties (and parameters) have a _required_ flag. Required information must be provide for successful data upload. Other (optional) information can be supplied at the discretion of the user.

## Visualisation

A very useful package for data wrangling is [pandas](https://pandas.pydata.org), also the [altair](https://altair-viz.github.io) visualisation package works well with pandas dataframes.

For example:

## Distribution

