import streamlit as st

import pandas as pd
import itkdb
import itkdb.exceptions as itkX

import pandas as pd
import altair as alt
import json

import sys
import os

# caution: path[0] is reserved for script path (or '' in REPL)
sys.path.insert(1, os.getcwd()+'/pdbApp/scripts')
from commonCode import GetPDBAccess, SelectComponentType, SelectTestType, EditSchema, FillCheckList, HighlightTests,GetStageInfo

### introduction
st.title(':pick: Loading half-ring')
st.write('### Load modules at your institute to a half-ring')

st.write("---")

### input user info.
st.write("## Get PDB Token")
st.write("Input user credentials to get token")

if "client" in st.session_state.keys():
    st.write("__token already found__")
else:
    GetPDBAccess(st.session_state)

if "client" not in st.session_state.keys():
    st.write("Please register credentials")
    st.stop()

st.write("---")

st.write("### Load half-ring")

client = st.session_state['client']
user = client.get("getUser", json={"userIdentity": client.user.identity})
user_location = user["institutions"][0]["code"]
st.write("Your institute is" , user["institutions"][0]["code"]) #if you have more than one, use default
st.write("Please select a half_ring at your institute")

data = {"project": "P", "subproject":"PE"}  
components_pixels = client.get("listComponents", json=data) 


#Module position lookup table (janky, probably better way to do this)
#way it works [layer number] : [HV1 modules] [HV2 modules]


lookupModPos = {2: [[5,6,7,8],[1,2,3,4]],
                3: [[7,8,9,10,11],[1,2,3,4,5,6]],
                4: [[8,9,10,11,12,13],[1,2,3,4,5,6,7]]}

relevantSN = [] #store half-ring objects
for component in components_pixels:
    
    if type is None: #make sure to parse out NoneType objects
        pass

    else:
        sn = str(component["serialNumber"])
        if "20UPELS" in sn:
            if component["state"] == "ready": #the other opetion is request to delete 
                #st.write(component["type"]["code"])
                location = component['institution']['code']
                if location == user_location: #collect at your institute
                    relevantSN.append(sn)

#Select module by SN selection box
sides = ['Side A', 'Side B']

if st.checkbox('Manually enter SN for half-ring?'):
    option = st.text_input('Please enter ATLAS SN for half-ring')
else:
    option = st.selectbox('Please select half-ring SN', (relevantSN))
layer = int(option[7])
mod_per_side = max(lookupModPos[layer][0]) #number of modules per side expected for the layer
selComponent = client.get("getComponent", json={"component":option})

sideSel = st.selectbox('Please select side of half-ring', (sides))

st.write('You have selected', sideSel)

if layer ==2:

    if sideSel == 'Side A':
        st.write('#### Your half-ring is layer 2, side A')
        st.write('###### You are expecting 8 modules on this side. It looks like this:')

        st.image("pdbApp/L2_HR_numbers.png")

    if sideSel == 'Side B':

        st.write('#### Your half-ring is layer 2, side B')
        st.write('###### You are expecting 8 modules on this side. It looks like this:')

        st.image("pdbApp/L2_HR_numbers.png")

if sideSel=='Side A':

    children = selComponent['children']
    orders = []
    ids = []
    sp_chain_A_sn = None
    for child in range(len(children)):
        kid = children[child]
        
        
        if kid['componentType']['code'] == 'OEC_LLS_SP': #identify SP chains

            if int(kid['order']) == 0:
                sp_chain_A_sn= kid['component']['serialNumber'] #get SN of HV_G
                
    sp_A = client.get("getComponent", json={"component":sp_chain_A_sn}) #get SP chain object so we can find its children

    HV_group_1 = None
    HV_group_2 = None

    hv_groups = sp_A['children']
    for group in range(len(hv_groups)):
        gp = hv_groups[group]
        hv_code = gp["type"]["code"]

        if 'HV1' in hv_code:

            hv1_sn = gp["component"]["serialNumber"]
            HV_group_1 = client.get("getComponent", json={"component":hv1_sn}) #get HV1 group using SN

        if 'HV2' in hv_code:

            hv2_sn = gp["component"]["serialNumber"]
            HV_group_2 = client.get("getComponent", json={"component":hv2_sn}) #get HV2 group using SN

    module_total = len(HV_group_1['children']) + len(HV_group_2['children'])
    remaining =  module_total - mod_per_side 

    st.write(f'## Total modules loaded: {module_total}/{mod_per_side}')

    if remaining == 0:

        st.write(f'## All modules loaded on {sideSel} :white_check_mark: ')

    if remaining > 0:
        remaining =  module_total - mod_per_side 
        st.write(f' You have {remaining} modules remaining')


if sideSel=='Side B':

    children = selComponent['children']
    sp_chain_B_sn= None
    for child in range(len(children)):
        kid = children[child]
        
        
        if kid['componentType']['code'] == 'OEC_LLS_SP': #identify SP chains
            if int(kid['order']) == 1:
                sp_chain_B_sn = kid['component']['serialNumber']
                
    sp_B = client.get("getComponent", json={"component":sp_chain_B_sn})

    HV_group_1 = None
    HV_group_2 = None

    hv_groups = sp_B['children']
    for group in range(len(hv_groups)):
        gp = hv_groups[group]
        hv_code = gp["type"]["code"]

        if 'HV1' in hv_code:

            hv1_sn = gp["component"]["serialNumber"]
            HV_group_1 = client.get("getComponent", json={"component":hv1_sn}) #get HV1 group using SN

        if 'HV2' in hv_code:

            hv2_sn = gp["component"]["serialNumber"]
            HV_group_2 = client.get("getComponent", json={"component":hv2_sn}) #get HV2 group using SN

    module_total = len(HV_group_1['children']) + len(HV_group_2['children'])
    remaining =  module_total - mod_per_side 
    st.write(f'## total modules loaded: {module_total}/{mod_per_side}')

    if remaining == 0:

        st.write(f'## All modules loaded on {sideSel} :white_check_mark: ')

    if remaining > 0:
        remaining =  module_total - mod_per_side 
        st.write(f' You have {remaining} modules remaining')

testTypes = client.get("listTestTypes", data={"project":"P", "componentType":"OEC_LLS"})

compTypeInfo=client.get('getComponentTypeByCode', json={'code':'OEC_LLS','project':'P'})
compInfo = client.get('getComponent', json={'component':option})
#     st.write(compTypeInfo)
### stages

st.write('## Move to next stage?')
df_stageList=pd.DataFrame(compTypeInfo['stages'])
df_stageList=df_stageList.rename(columns={k:'stage_'+k for k in ['code','name','order','alternative','initial','final']})

stageList= df_stageList['stage_code'].tolist()
stageTests = df_stageList['testTypes']

currentStage = selComponent['currentStage']['code']
st.write('stage',currentStage)
stageTests_codes = []
stageTests_names = []

for stage in range(len(stageList)):

    if currentStage == stageList[stage]:
    
        st.write('index', stage)

        assTest = stageTests[stage]

        if len(assTest) > 0:

            for tt in range(len(assTest)):

                stageTests_codes.append(assTest[tt]['testType']['code'])
                stageTests_names.append(assTest[tt]['testType']['name'])

        else:
            st.write('There are no test types associated with this stage')
st.write('The tests associated with this stage are:', stageTests_names)


tests_done_id = [] #collect IDs of collected tests

for test in selComponent['tests']:

    name = test["code"]
    for code in range(len(stageTests_codes)):

        if stageTests_codes[code] == name:

            st.write('MATCH!', name) #have you done this test before?

            for run in test["testRuns"]:
                 if run["state"] == "ready":
                     if run["passed"] == True:
                         st.write(f":white_check_mark: Component passed {name} on {run['stateTs']}")

                     elif run["passed"] == False:
                         
                         st.write(f":no_entry_sign: Component failed {name} on {run['stateTs']}")


stage_name = st.selectbox('select stage', options=df_stageList['stage_code'])















