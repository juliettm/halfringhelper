import streamlit as st

import os
import subprocess


### introduction
st.title('Preliminary Half-RingHelper')
st.write("---")

### contents
st.write("## Contents")
pyFiles=[f for f in os.listdir(os.getcwd()+"/pdbApp/pages") if ".py" in f] 
pyFiles=sorted(pyFiles)
for e,pf in enumerate(pyFiles,1):
    st.write(f" {e}. {pf}")

st.write("---")

### links
st.write("## Useful Links")
linkList=[
    {'name':"itk-docs", 'link':"https://itk.docs.cern.ch"},
    {'name':"PDB API docs", 'link':"https://uuapp.plus4u.net/uu-bookkit-maing01/78462435-41f76117152c4c6e947f498339998055/book/page?code=11425791"},
    {'name':"itkdb docs", 'link':"https://pypi.org/project/itkdb/"},
]
for ll in linkList:
    st.write(f"[{ll['name']}]({ll['link']})")

st.write("---")
