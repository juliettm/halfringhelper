import streamlit as st

import pandas as pd
import itkdb
import itkdb.exceptions as itkX

import sys
import os

# caution: path[0] is reserved for script path (or '' in REPL)
sys.path.insert(1, os.getcwd()+'/pdbApp/scripts')
from commonCode import GetPDBAccess, GetPopulation, SelectComponentType, SelectTestType, EditSchema


### introdHalf-Ring Info')
st.write("---")

### input user info.
st.write("## Get PDB Token")
st.write("Input user credentials to get token")

if "client" in st.session_state.keys():
    st.write("__token already found__")
else:
    GetPDBAccess(st.session_state)

if "client" not in st.session_state.keys():
    st.write("Please register credentials")
    st.stop()

st.write("---")


st.write("## Available half-rings")

# get all componentTypes then filter
# if "compTypeList" not in st.session_state.keys():
#     st.session_state['compTypeList']=st.session_state['client'].get('listComponentTypes', json={'project':st.session_state['project'][0], 'componentType':"MODULES" }).data
client = st.session_state['client']

data = {"project": "P", "subproject":"PE"}  
components_pixels = client.get("listComponents", json=data)  

print(components_pixels.total)  
relevantSN = [] #store half-ring objects
for component in components_pixels:
    
    if type is None: #make sure to parse out NoneType objects
        pass

    else:
        sn = str(component["serialNumber"])
        if "20UPELS" in sn:
            if component["state"] == "ready": #the other opetion is request to delete 
                #st.write(component["type"]["code"])
                relevantSN.append(sn)

#Select module by SN selection box
sides = ['Side A', 'Side B']
option = st.selectbox('Please select endcap SN', (relevantSN))
selComponent = client.get("getComponent", json={"component":option})
print(selComponent)
layer = int(option[7])

selSide  = st.selectbox('Select side:', (sides))

if layer ==2:

    if selSide == 'Side A':
        st.write('#### Your half-ring is layer 2, side A')
        st.write('###### You are expecting 8 modules on this side. It looks like this:')

        st.image("pdbApp/L2_endcap_prelim.jpg")
    if selSide == 'Side B':

        st.write('#### Your half-ring is layer 2, side B')
        st.write('###### You are expecting 8 modules on this side. It looks like this:')

        st.image("pdbApp/L2_endcap_prelim.jpg")

elif layer == 3:

    st.write('your endcap is layer 3')

elif layer == 4:

    st.write('Your endcap is layer 4')

else:
    st.write('There has been an error reading the SN. End-cap layer should be 2,3,4.')

if st.checkbox('See more info?'):
    st.write('##### Some info:')
    st.write(f"###### object location: {selComponent['currentLocation']['name']}")
    st.write(f"###### current stage: {selComponent['currentStage']['name']}")


