import streamlit as st
import json
import itkdb
import itkdb.exceptions as itkX
import pandas as pd
import ast

### get PDB credentials
def GetPDBAccess(ssObj):
    st.write("### Input Credentials")
    # cache credentials in dictionary
    if "userTokens" not in ssObj:
        ssObj['userTokens']={'ac1':None,'ac2':None}

    radSel=st.radio("Input selection:",["file","keyboard"])

    if radSel=="keyboard":
        st.write("Please input tokens via keyboard")
        ssObj['userTokens']['ac1']=st.text_input("first access token", type="password")
        ssObj['userTokens']['ac2']=st.text_input("second access token", type="password")
    else:
        st.write("Please input tokens via file")
        st.json({'ac1':"YOUR_AC1",'ac2':"YOUR_AC2"})
        ## drag and drop method
        upFile = st.file_uploader('Upload JSON file', type="json")
        if upFile is not None:
            if st.checkbox("Check upload?"):
                st.write("uploaded file:",upFile.getvalue().decode('utf-8'))
            credDict = json.load(upFile)
            # format keys (use same dictionary)
            for ac in ["AC1","AC2"]:
                if ac.lower() in [k.lower() for k in credDict.keys()]:
                    try:
                        ssObj['userTokens'][ac.lower()]=credDict[ac.lower()]
                    except KeyError:
                        ssObj['userTokens'][ac.lower()]=credDict[ac]
                    except:
                        st.write(f"Cannot find {ac} in file")
                else:
                    st.write("No file uploaded")
                    st.download_button(label="Download example JSON", data=json.dumps({'ac1':"YOUR_AC1",'ac2':"YOUR_AC2"}, indent=2), file_name="credentials.json")

    ### ready to get token
    if ssObj['userTokens']['ac1']!=None and ssObj['userTokens']['ac2']!=None:
        st.write("Credentials found!")
        # debug option
        if st.checkbox("Check credentials?"):
            st.json(ssObj['userTokens'])
        # get token
        if st.button("Get PDB token!"):
            st.write("use tokens to get client")
            ssObj['user'] = itkdb.core.User(access_code1=ssObj['userTokens']['ac1'], access_code2=ssObj['userTokens']['ac2'])
            try:
                ssObj['user'].authenticate()
                ssObj['client'] = itkdb.Client(user=ssObj['user'])
                st.success("Got PDB Token!")
            except itkX.ResponseException:
                st.error("Cannot authenticate user credentials")
                st.stop()
    else:
        st.stop()

### select componentType
def SelectComponentType(ssObj):
    # get all componentTypes
    if "compTypeList" not in ssObj.keys():
        ssObj['compTypeList']=ssObj['client'].get('listComponentTypes', json={} ).data

    # convert to dataframe
    df_compTypes=pd.DataFrame(ssObj['compTypeList'])
    # unpack project dictionaries
    df_compTypes['project_code']=df_compTypes['project'].apply(lambda x: x['code'] if type(x)==type({}) and "code" in x.keys() else x)
    df_compTypes['project_name']=df_compTypes['project'].apply(lambda x: x['name'] if type(x)==type({}) and "name" in x.keys() else x)

    # debug check
    if st.checkbox("Check componentType list?"):
        st.write(df_compTypes)

    # select project
    selProj=st.selectbox("Select project (name):",options=df_compTypes['project_code'].unique())
    # st.write(selProj)

    # select componentType (of project)
    selComp=st.selectbox("Select componentType (name):",options=df_compTypes.query('project_code=="'+selProj+'"')['code'].unique())
    # st.write(selComp)

    # selected 
    df_ct=df_compTypes.query('code=="'+selComp+'" & project_code=="'+selProj+'"').reset_index(drop=True)
    st.write(df_ct)

    return df_ct

### select componentType
def SelectTestType(ssObj):
    # get all testTypes
    if "testTypeList" not in ssObj.keys():
        ssObj['testTypeList']=ssObj['client'].get('listTestTypes', json={} ).data

    # convert to dataframe
    df_testTypes=pd.DataFrame(ssObj['testTypeList'])
    # unpack project dictionaries
    df_testTypes['componentType_code']=df_testTypes['componentType'].apply(lambda x: x['code'] if type(x)==type({}) and "code" in x.keys() else x)
    df_testTypes['componentType_name']=df_testTypes['componentType'].apply(lambda x: x['name'] if type(x)==type({}) and "name" in x.keys() else x)
    df_testTypes['project_code']=df_testTypes['componentType'].apply(lambda x: x['project'] if type(x)==type({}) and "project" in x.keys() else x)

    # debug check
    if st.checkbox("Check testType list?"):
        st.write(df_testTypes)

    # select project
    selProj=st.selectbox("Select project (code):",options=df_testTypes['project_code'].unique())
    # st.write(selProj)

    # select componentType (of project)
    selComp=st.selectbox("Select componentType (name):",options=df_testTypes.query('project_code=="'+selProj+'"')['componentType_name'].unique())
    # st.write(selComp)

    # select testType (of componentType)
    selTest=st.selectbox("Select testType (name):",options=df_testTypes.query('componentType_name=="'+selComp+'" & project_code=="'+selProj+'"')['name'].unique())
    # st.write(selTest)

    # selected 
    df_tt=df_testTypes.query('name=="'+selTest+'" & componentType_name=="'+selComp+'" & project_code=="'+selProj+'"').reset_index(drop=True)
    st.write(df_tt)

    return df_tt

### select componentType
def SelectTestTypeOEC(ssObj):
    # get all testTypes
    if "testTypeList" not in ssObj.keys():
        ssObj['testTypeList']=ssObj['client'].get('listTestTypes', json={} ).data

    # convert to dataframe
    df_testTypes=pd.DataFrame(ssObj['testTypeList'])
    # unpack project dictionaries
    df_testTypes['componentType_code']=df_testTypes['componentType'].apply(lambda x: x['code'] if type(x)==type({}) and "code" in x.keys() else x)
    df_testTypes['componentType_name']=df_testTypes['componentType'].apply(lambda x: x['name'] if type(x)==type({}) and "name" in x.keys() else x)
    df_testTypes['project_code']=df_testTypes['componentType'].apply(lambda x: x['project'] if type(x)==type({}) and "project" in x.keys() else x)

    # debug check
    if st.checkbox("Check testType list?"):
        st.write(df_testTypes)

    # select project
    selProj=st.selectbox("Select project (code):",options=['P'])
    # st.write(selProj)

    # select componentType (of project)
    #selComp=st.selectbox("Select componentType (name):",options=df_testTypes.query('project_code=="'+selProj+'"')['componentType_name'].unique())
    selComp=st.selectbox("Select componentType (name):",options=["OEC_LLS_SP"])#df_testTypes.query('project_code=="'+selProj+'"')['componentType_code'].unique())
    # st.write(selComp)

    # select testType (of componentType)
    selTest=st.selectbox("Select testType (name):",options=df_testTypes.query('componentType_code=="'+selComp+'" & project_code=="'+selProj+'"')['name'].unique())
    # st.write(selTest)

    # selected 
    df_tt=df_testTypes.query('name=="'+selTest+'" & componentType_code=="'+selComp+'" & project_code=="'+selProj+'"').reset_index(drop=True)
    st.write(df_tt)

    return df_tt

def GetTestInfo(compInfo):
    
    if "tests" not in compInfo.keys() or len(compInfo['tests'])==0:
        st.write("__No test information or component__")
        return pd.DataFrame()
    # st.write(compInfo['tests'])
    df_tests=pd.DataFrame(compInfo['tests'])
    df_tests=df_tests.explode('testRuns').reset_index(drop=True)
    for col in ['id','state','runNumber','passed','problems','date']:
        df_tests['testRun_'+col]=df_tests['testRuns'].apply(lambda x: x[col] if type(x)==type({}) and col in x.keys() else None)
    df_tests['testRun_inst']=df_tests['testRuns'].apply(lambda x: x['institution']['code'] if type(x)==type({}) and "institution" in x.keys() and type(x['institution'])==type({}) and "code" in x['institution'].keys() else None)
    ### remove deleted
    df_tests=df_tests.query('testRun_state=="ready"')
    # st.write(df_tests)
    ### format date and ignore non-standard formats
    df_tests['testRun_date']=pd.to_datetime(df_tests['testRun_date'], format='%Y-%m-%dT%H:%M:%S.%fZ', errors="ignore")
    df_tests=df_tests.sort_values(by=["code","testRun_passed","testRun_date"])
    ### consolidate
    df_tests=df_tests.groupby(by=["code","testRun_passed"]).last().reset_index()
    return df_tests

### populate checklist
def FillCheckList(compInfo):
    
    df_tests=GetTestInfo(compInfo)
    if df_tests.empty:
        st.write("Cannot check previous uploads")
        return pd.DataFrame()
    df_checkList=GetCheckList(compInfo)
    
    df_checkList['present']="MISSING"
    df_checkList['passed']=False
    for index,row in df_checkList.iterrows():
        tc=row['test_code']
        # find test
        if not df_tests.query(f'code=="{tc}"').empty:
            df_checkList.at[index, 'present']="EXISTS"
        # find test and passed
        if not df_tests.query(f'code=="{tc}" & testRun_passed==True').empty:
            df_checkList.at[index, 'passed']=True
        # find current test
        if row['stage_code']==compInfo['currentStage']['code']:
            df_checkList.at[index, 'present']+="_currentStage"
    
    return df_checkList

def GetCheckList(compInfo):
    #compTypeInfo=DBaccess.DbGet(st.session_state.myClient,'getComponentTypeByCode', {'code':compInfo['componentType']['code'],'project':compInfo['project']['code']}) 
    compTypeInfo = compInfo['client'].get('getComponentTypeByCode', json={'code':compInfo['componentType']['code'],'project':compInfo['project']['code']} )
#     st.write(compTypeInfo)
    ### stages
    df_stageList=pd.DataFrame(compTypeInfo['stages'])
    df_stageList=df_stageList.rename(columns={k:'stage_'+k for k in ['code','name','order','alternative','initial','final']})
#     df_stageList
    ### then tests
    df_checkList=df_stageList.explode('testTypes').reset_index(drop=True)
    df_checkList.iloc[0]['testTypes']
    for col in ['order','nextStage','receptionTest','receptionTestOnly']:
        df_checkList['test_'+col]=df_checkList['testTypes'].apply(lambda x: x[col] if type(x)==type({}) and col in x.keys() else None)
    df_checkList['test_code']=df_checkList['testTypes'].apply(lambda x: x['testType']['code'] if type(x)==type({}) and "testType" in x.keys() and type(x['testType'])==type({}) and "code" in x['testType'].keys() else None)
#     df_checkList

    ### drop receptionOnly tests
    df_checkList=df_checkList.query('test_receptionTestOnly!=True').reset_index(drop=True)

    return df_checkList


### highlighting
def HighlightTests(s):
    if s['passed'] and s['present']=="EXISTS":
        return ['background-color: lightgreen'] * len(s)
    elif s['passed'] and "currentStage" in s['present']:
        return ['background-color: green'] * len(s)
    elif not s['passed'] and s['present']=="EXISTS":
        return ['background-color: pink'] * len(s)
    elif not s['passed'] and "currentStage" in s['present']:
        return ['background-color: red'] * len(s)
    else:
        return ['text-color: black'] * len(s)


def Tryeval(val):
    # https://stackoverflow.com/questions/2859674/how-to-convert-list-of-strings-to-their-correct-python-types
    try:
        val = ast.literal_eval(val)
    except ValueError:
        pass
    except SyntaxError:
        pass
    return val


### try to match stuff
def SelectCheck(k,v, inJson, inKey=None):
    try:
        if "true" in v.lower() or "false" in v.lower():
            val=st.radio(k+" :", [True, False], index=["True", "False"].index(v), key=inKey)
        else:
            val=st.text_input(k+" :", value=str(v), max_chars=None, type='default', key=inKey)
            if "[" and "]" in val or "{" and "}" in val:
                val=ast.literal_eval(val)
    except AttributeError: # non string types will not have lower() attribute
        if type(v)==type(True):
            val=st.radio(k+" :", [True, False], index=[True, False].index(v), key=inKey)
        else:
            val=st.text_input(k+" :", value=str(v), max_chars=None, type='default', key=inKey)
            if "[" and "]" in val or "{" and "}" in val:
                val=ast.literal_eval(val)
    # inVal=Tryeval(val)
    # inVal=stTrx.MatchType(v,inVal)
    return val #inVal

def GetStageInfo(compInfo):
    df_tests=pd.DataFrame(compInfo['tests'])
    df_tests=df_tests.explode('testRuns').reset_index(drop=True)
    for col in ['id','state','runNumber','passed','problems','date']:
        df_tests['testRun_'+col]=df_tests['testRuns'].apply(lambda x: x[col] if type(x)==type({}) and col in x.keys() else None)
    df_tests['testRun_inst']=df_tests['testRuns'].apply(lambda x: x['institution']['code'] if type(x)==type({}) and "institution" in x.keys() and type(x['institution'])==type({}) and "code" in x['institution'].keys() else None)
    ### remove deleted
    df_tests=df_tests.query('testRun_state=="ready"')
    df_tests['testRun_date']=pd.to_datetime(df_tests['testRun_date'], format='%Y-%m-%dT%H:%M:%S.%fZ')
    df_tests=df_tests.sort_values(by=["code","testRun_passed","testRun_date"])
    ### consolidate
    df_tests=df_tests.groupby(by=["code","testRun_passed"]).last().reset_index()


### edit schema json
def EditSchema(ssObj):
    if st.checkbox("Edit Schema?"):
        
        for k,v in ssObj['objSchema'].items():
            if type(v)==type({}):
                st.write("**"+str(k)+"**")
                for l,w in v.items():
                    # inJson[k][l]=SelectCheck(l,w)
                    ssObj['objSchema'][k][l]=SelectCheck(l,w,ssObj['objSchema'])
            else:
                # inJson[k]=SelectCheck(k,v)
                ssObj['objSchema'][k]=SelectCheck(k,v,ssObj['objSchema'])

    else:
        st.write(ssObj['objSchema'])


### get population
def GetPopulation(myClient, projCode, compTypeCode, propCode):

    st.write("Getting population count...")
    total=myClient.get('getComponentCount', json={'project':projCode,'componentType':compTypeCode})['count']
    pageSize=50
    count= int(total/pageSize)
    if total%pageSize>0:
        count=count+1
    
    st.write(f" - ... total population count: {total}")

    ### make list of altIDs and SNs
    st.write(f"Retrieving components...")
    my_bar = st.progress(0)
    popList=[]
    for pi in range(0,count,1):
        # print(f"loop: {pi+1}/{count}")
        my_bar.progress( float(pi+1)/count)
        compList=myClient.get('listComponents', json={'project':"P",'componentType':compTypeCode,'pageInfo':{ 'pageIndex': pi, 'pageSize': pageSize }})
        # print(f'length: {len(compList.data)}')
        popList.extend( #compList.data)
            [ {'alternativeIdentifier':comp['alternativeIdentifier'], 'serialNumber':comp['serialNumber'], 'type':comp['type']['code'], 'property':prop['value']} for comp in compList.data for prop in comp['properties'] if prop['code']==propCode ] )
    else:
        st.write("Retrieval complete")
    
    return popList


